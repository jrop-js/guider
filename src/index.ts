export type Iteratee<T> = (x: T) => string
const head = <T>(array: T[]): T => (array.length > 0 ? array[0] : undefined)

export default function index<T>(array: T[], _iteratee: string | Iteratee<T>) {
	// from array, create:
	// {
	//   [key]: [indexes of match]
	// }
	let iteratee: Iteratee<T> = _iteratee as Iteratee<T>
	if (typeof _iteratee == 'string') {
		const key = _iteratee
		iteratee = x => x[key]
	}

	const idx = {}
	array.forEach((item, i) => {
		const idxKey = iteratee(item)
		if (typeof idx[idxKey] == 'undefined') idx[idxKey] = []
		idx[idxKey].push(i)
	})

	const find = (key: string): T[] => {
		const indices = idx[key] || []
		return indices.map(i => array[i])
	}
	const first = (key: string): T => head(find(key))
	const has = (key: string): boolean => (idx[key] || []).length > 0
	const one = (key: string): T => {
		const elements = find(key)
		if (elements.length != 1)
			throw new Error(
				`Expected exactly one element to match ${JSON.stringify(key)}: found ${
					elements.length
				}`
			)
		return head(elements)
	}
	return Object.assign(find, {find, first, has, one})
}
