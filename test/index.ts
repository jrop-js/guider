import guider from '../src'
import test = require('tape')

const DATA = [
	{
		name: 'Rodger',
		age: 25,
	},
	{
		name: 'Rodriguez',
		age: 30,
	},
	{
		name: 'Gomer',
		age: 25,
	},
	{
		name: 'Shia',
		age: 89,
	},
]

test('guider', t => {
	let index = guider(DATA, 'age')
	t.deepLooseEqual(
		index.find('25').map(i => i.name),
		['Rodger', 'Gomer'],
		'finds 25s (string iteratee)'
	)

	index = guider(DATA, i => i.age.toString())
	t.deepLooseEqual(
		index.find('25').map(i => i.name),
		['Rodger', 'Gomer'],
		'finds 25s'
	)

	t.equal(index.first('25').name, 'Rodger', 'first 25')
	t.equal(index.one('89').name, 'Shia', 'one 89')
	t.throws(() => index.one('25'), 'one 25 (throws)')
	t.throws(() => index.one('N/A'), 'one N/A (throws)')

	t.equal(index.has('89'), true, 'contains 89')
	t.equal(index.has('N/A'), false, 'does not contain N/A')
	t.end()
})
