# guider

> Index JavaScript arrays for fast searching

[![pipeline status](https://gitlab.com/jrop-js/guider/badges/master/pipeline.svg)](https://gitlab.com/jrop-js/guider/commits/master)

## Installation

```sh
npm install --save guider
```

## Use

The unit tests for this library describe the use pretty comprehensively:

```js
import guider from 'guider'
import test = require('tape')

const DATA = [
	{
		name: 'Rodger',
		age: 25,
	},
	{
		name: 'Rodriguez',
		age: 30,
	},
	{
		name: 'Gomer',
		age: 25,
	},
	{
		name: 'Shia',
		age: 89,
	},
]

test('guider', t => {
	let index = guider(DATA, 'age')
	t.deepLooseEqual(
		index.find('25').map(i => i.name),
		['Rodger', 'Gomer'],
		'finds 25s (string iteratee)'
	)

	index = guider(DATA, i => i.age.toString())
	t.deepLooseEqual(
		index.find('25').map(i => i.name),
		['Rodger', 'Gomer'],
		'finds 25s'
	)

	t.equal(index.first('25').name, 'Rodger', 'first 25')
	t.equal(index.one('89').name, 'Shia', 'one 89')
	t.throws(() => index.one('25'), 'one 25 (throws)')
	t.throws(() => index.one('N/A'), 'one N/A (throws)')

	t.equal(index.has('89'), true, 'contains 89')
	t.equal(index.has('N/A'), false, 'does not contain N/A')
	t.end()
})
```

## License (ISC)

Copyright 2017 <jrapodaca@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
